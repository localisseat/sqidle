﻿import {InitializeGame} from "./InitializeGame.js";
import {UpdateUI} from "./UpdateUI.js";
import {Upgrade} from "./Upgrade.js";

const CurrenciesArraySize = 256;
const BuildingArraySize = 256;

/**
 * Глобальные данные для упрощенной работы с import
 * @type {{currenciesProfit: *[], buildingsInstances: *}}
 */
export let globalGameData = {
    /**
     * Текущий профит доход-расход от каждого материала
     * @type {*[]}
     */
    currenciesProfit: new Array(CurrenciesArraySize).fill(0),
    /**
     * Массив классов построек
     * @type {*[instance]}
     */
    buildingsInstances: [],
}

/**
 * Данные которые сохраняются
 * @type {{BuildingsLevel: any[], Currencies: any[]}}
 */
export let savebleGameData =
    {
        Currencies: new Array(CurrenciesArraySize).fill(0),
        /**
         * Текущий лвл постройки
         */
        BuildingsLevel: new Array(BuildingArraySize).fill(0),
        /**
         * Максимальный лвл постройки
         */
        BuildingsMaxLevel: new Array(BuildingArraySize).fill(0),
    }

InitializeGame();

setInterval(endOfTurnCalc, 1000);

function endOfTurnCalc() {
    for (let i; i < globalGameData.buildingsInstances.length; i++) {
        globalGameData.buildingsInstances[i].Execute();
    }

    UpdateUI();
    AutoBuy();
}

function AutoBuy() {
    let buildings = globalGameData.buildingsInstances;
    for (let i = 0; i < buildings.length; i++) {
        let element = document.getElementById(`autoBuy${buildings[i].id}`);
        if (element.checked) {
            Upgrade(buildings[i].instanceID);
        }
    }
}