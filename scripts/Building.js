import {globalGameData, savebleGameData} from "./script.js";
import {
    SetInstanceCurrentPlusLvl, SetInstanceCurrentMinusLvl,
    Upgrade,
    CheckSetInstanceCurrentMinusLvl, CheckSetInstanceCurrentPlusLvl
} from "./Upgrade.js";
import {CurrencyEnum} from "./MaterialsEnum.js";

export class Building {
    upgradeButton;
    inputAutoBuy;
    levelSpan;
    increaseSpan;
    decreaseSpan;
    upgradeCostSpan;
    rowNumberId = 0;
    increaseMultiplier = 0.99;
    decreaseMultiplier = 0.99;
    costUpgradeMultiplier = 1.05;

    constructor(name, id, buildingType) {
        this.name = name;
        this.id = id;
        this.buildingType = buildingType;
        this.instanceID = globalGameData.buildingsInstances.length;
        globalGameData.buildingsInstances.push(this);

        this.increases = [];
        this.baseIncreases = [];
        this.decreases = [];
        this.baseDecreases = [];
        this.upgradeCost = [];
        this.baseUpgradeCost = [];
        this.buyCost = [];
    }

    UpdateUI() {
        let isEnough = true;

        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.upgradeCost[i] > savebleGameData.Currencies[i]) {
                isEnough = false;
            }
        }
        this.inputAutoBuy = document.getElementById("autoBuy" + this.id);
        let equalLvl = savebleGameData.BuildingsLevel[this.buildingType] === savebleGameData.BuildingsMaxLevel[this.buildingType];
        if (isEnough === true && equalLvl) {
            this.upgradeButton.disabled = false;
        } else {
            this.upgradeButton.disabled = true;
        }
        if (equalLvl) {
            this.inputAutoBuy.disabled = false;
            
        } else {
            this.inputAutoBuy.disabled = true;
        }

        let buttonMinus = document.getElementById(`buttonMinus${this.id}`);
        let buttonPlus = document.getElementById(`buttonPlus${this.id}`);
        
        if (CheckSetInstanceCurrentMinusLvl(this.buildingType)) {
            buttonMinus.disabled = false;
        } else {
            buttonMinus.disabled = true;
        }
        
        if (CheckSetInstanceCurrentPlusLvl(this.buildingType)) {
            buttonPlus.disabled = false;
        } else {
            buttonPlus.disabled = true;
        }
        
        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.increases[i] > 0) {
                this.increaseSpan = document.getElementById(this.id + "Increase" + i.toString());
                this.increaseSpan.innerHTML = ConvertCurrencyIdToString(i) + ": " + this.increases[i].toFixed(1) + "/сек";
                this.pickedSpan = document.getElementById(this.id + "IncreaseCurrent" + i.toString());
                if (globalGameData.currenciesProfit[i] >= 0) {
                    this.pickedSpan.className = "green"
                } else {
                    this.pickedSpan.className = "red horizontal-shake"
                }
                this.pickedSpan.innerHTML = " (" + globalGameData.currenciesProfit[i].toFixed(1) + ")";
            }
            if (this.decreases[i] > 0) {
                this.decreaseSpan = document.getElementById(this.id + "Decrease" + i.toString());
                this.decreaseSpan.innerHTML = ConvertCurrencyIdToString(i) + ": " + this.decreases[i].toFixed(1) + "/сек";
                this.pickedSpan = document.getElementById(this.id + "DecreaseCurrent" + i.toString());
                if (globalGameData.currenciesProfit[i] >= 0) {
                    this.pickedSpan.className = "green"
                } else {
                    this.pickedSpan.className = "red horizontal-shake"
                }
                this.pickedSpan.innerHTML = " (" + globalGameData.currenciesProfit[i].toFixed(1) + ")";
            }
            if (this.upgradeCost[i] > 0) {
                this.upgradeCostSpan = document.getElementById(this.id + "UpgradeCost" + i.toString());
                this.upgradeCostSpan.innerHTML = ConvertCurrencyIdToString(i) + ": " + this.upgradeCost[i].toFixed(1);
                this.pickedSpan = document.getElementById(this.id + "UpgradeCostCurrent" + i.toString());
                if (this.upgradeCost[i] <= savebleGameData.Currencies[i]) {
                    this.pickedSpan.className = "green"
                } else {
                    this.pickedSpan.className = "red"
                }
                this.pickedSpan.innerHTML = " (" + savebleGameData.Currencies[i].toFixed(1) + ")";
            }
        }

        this.levelSpan.innerHTML = savebleGameData.BuildingsLevel[this.buildingType];
    }

    Execute() {
        if (savebleGameData.BuildingsLevel[this.buildingType] <= 0) {
            return;
        }
        let isEnough = true;

        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.decreases[i] > savebleGameData.Currencies[i]) {
                isEnough = false;
            }
        }

        if (isEnough === true) {
            for (let i = 0; i < savebleGameData.Currencies.length; i++) {
                if (this.increases[i] > 0) {
                    savebleGameData.Currencies[i] += this.increases[i];
                }
            }

            for (let i = 0; i < savebleGameData.Currencies.length; i++) {
                if (this.decreases[i] > 0) {
                    savebleGameData.Currencies[i] -= this.decreases[i];
                }
            }
        }
    }

    CreateHTML() {
        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            this.increases[i] = 0;
            this.decreases[i] = 0;
            this.upgradeCost[i] = 0;
            
            if (this.buyCost[i] > 0) {
                this.upgradeCost[i] = this.buyCost[i];
            }
        }

        const produceChainDiv = document.getElementById(`rowProduceChain${this.rowNumberId}`);
        const fragment = document.createDocumentFragment();

        const divTag = document.createElement("div");
        divTag.className = "flex-item " + this.id;

        divTag.append(this.name);
        divTag.append(document.createElement("br"));

        let buttonTag = document.createElement("button");
        buttonTag.className = "main-upgrade";
        buttonTag.id = "button" + this.id;
        let classInst = this.instanceID;
        buttonTag.addEventListener('click', function () {
            Upgrade(classInst);
        });
        buttonTag.textContent = "Улучшить";
        divTag.appendChild(buttonTag);

        const inputTag = document.createElement("input");
        inputTag.type = "checkbox";
        inputTag.id = "autoBuy" + this.id;
        inputTag.checked = false;
        divTag.appendChild(inputTag);
        divTag.append(document.createElement("br"));

        buttonTag = document.createElement("button");
        buttonTag.className = 'buttonLvl'
        buttonTag.id = "buttonMinus" + this.id;
        buttonTag.addEventListener('click', function () {
            SetInstanceCurrentMinusLvl(classInst);
        });
        buttonTag.textContent = "−";
        divTag.appendChild(buttonTag);

        divTag.append("Уровень: ");
        let spanTag = document.createElement("span");
        spanTag.id = "level" + this.id;
        spanTag.textContent = "0";
        divTag.append(spanTag);

        buttonTag = document.createElement("button");
        buttonTag.className = 'buttonLvl'
        buttonTag.id = "buttonPlus" + this.id;
        buttonTag.addEventListener('click', function () {
            SetInstanceCurrentPlusLvl(classInst);
        });
        buttonTag.textContent = "+";
        divTag.appendChild(buttonTag);

        divTag.append(document.createElement("br"));
        divTag.append(document.createElement("p"));

        divTag.append("Стоимость покупки/улучшения:");
        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.baseUpgradeCost[i] > 0) {
                divTag.append(document.createElement("br"));
                let spanTagUpgrade = document.createElement("span");
                spanTagUpgrade.id = this.id + "UpgradeCost" + i.toString();
                spanTagUpgrade.textContent = ConvertCurrencyIdToString(i) + ": " + this.upgradeCost[i];
                divTag.append(spanTagUpgrade);
                let spanTagCurrent = document.createElement("span");
                spanTagCurrent.id = this.id + "UpgradeCostCurrent" + i.toString();
                spanTagCurrent.textContent = " (" + savebleGameData.Currencies[i] + ")";
                divTag.append(spanTagCurrent);
            }
        }

        divTag.append(document.createElement("p"));
        divTag.append("Прирост:");

        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.baseIncreases[i] > 0) {
                divTag.append(document.createElement("br"));
                let spanTagUpgrade = document.createElement("span");
                spanTagUpgrade.id = this.id + "Increase" + i.toString();
                spanTagUpgrade.textContent = ConvertCurrencyIdToString(i) + ": " + this.increases[i] + "/сек";
                divTag.append(spanTagUpgrade);
                let spanTagCurrent = document.createElement("span");
                spanTagCurrent.id = this.id + "IncreaseCurrent" + i.toString();
                spanTagCurrent.textContent = " (" + savebleGameData.Currencies[i] + ")";
                divTag.append(spanTagCurrent);
            }
        }
        divTag.append(document.createElement("p"));
        divTag.append("Расход:");

        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.baseDecreases[i] > 0) {
                divTag.append(document.createElement("br"));
                let spanTagUpgrade = document.createElement("span");
                spanTagUpgrade.id = this.id + "Decrease" + i.toString();
                spanTagUpgrade.textContent = ConvertCurrencyIdToString(i) + ": " + this.decreases[i] + "/сек";
                divTag.append(spanTagUpgrade);
                let spanTagCurrent = document.createElement("span");
                spanTagCurrent.id = this.id + "DecreaseCurrent" + i.toString();
                spanTagCurrent.textContent = " (" + savebleGameData.Currencies[i] + ")";
                divTag.append(spanTagCurrent);
            }
        }

        fragment.appendChild(divTag);
        produceChainDiv.appendChild(fragment);

        this.inputAutoBuy = document.getElementById("autoBuy" + this.id);
        this.upgradeButton = document.getElementById("button" + this.id);
        this.levelSpan = document.getElementById("level" + this.id);
        this.upgradeButton.disabled = true;
    }

    UpdateLevelValues() {
        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            if (this.baseIncreases[i] > 0) {
                this.increases[i] = this.baseIncreases[i] * Math.pow(savebleGameData.BuildingsLevel[this.buildingType], this.increaseMultiplier);
            }
            if (this.baseDecreases[i] > 0) {
                this.decreases[i] = this.baseDecreases[i] * Math.pow(savebleGameData.BuildingsLevel[this.buildingType], this.decreaseMultiplier);
            }
            if (this.baseUpgradeCost[i] > 0) {
                this.upgradeCost[i] = this.baseUpgradeCost[i] * Math.pow(savebleGameData.BuildingsLevel[this.buildingType], this.costUpgradeMultiplier);
            }
        }
    }
}

function ConvertCurrencyIdToString(currency) {
    if (currency === CurrencyEnum.Copper) {
        return "Медная руда";
    }
    if (currency === CurrencyEnum.CopperIngot) {
        return "Медный слиток";
    }
    if (currency === CurrencyEnum.CopperGear) {
        return "Медная шестерня";
    }
    if (currency === CurrencyEnum.Silver) {
        return "Серебрянная руда";
    }
    if (currency === CurrencyEnum.SilverIngot) {
        return "Серебрянный слиток";
    }
}