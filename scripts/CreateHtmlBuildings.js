import {Building} from "./Building.js";
import {CurrencyEnum, BuildingsEnum, BuildingsNameEnum, BuildingsStringEnum} from "./MaterialsEnum.js";

export function CreateHtmlBuildings() {
    //#region Copper
    let building = new Building(BuildingsNameEnum.CopperMine, BuildingsStringEnum.CopperMine, BuildingsEnum.CopperMine);
    building.baseIncreases[CurrencyEnum.Copper] = 1;
    building.baseUpgradeCost[CurrencyEnum.Copper] = 10;
    building.buyCost[CurrencyEnum.Copper] = 10;
    building.rowNumberId = 0;
    building.increaseMultiplier = 0.99;
    building.decreaseMultiplier = 0.99;
    building.costUpgradeMultiplier = 1.05;
    building.CreateHTML();

    building = new Building(BuildingsNameEnum.CopperSmelter, BuildingsStringEnum.CopperSmelter, BuildingsEnum.CopperSmelter);
    building.baseIncreases[CurrencyEnum.CopperIngot] = 1;
    building.baseDecreases[CurrencyEnum.Copper] = 1;
    building.baseUpgradeCost[CurrencyEnum.Copper] = 10;
    building.baseUpgradeCost[CurrencyEnum.CopperIngot] = 20;
    building.buyCost[CurrencyEnum.Copper] = 100;
    building.rowNumberId = 0;
    building.increaseMultiplier = 0.90;
    building.decreaseMultiplier = 0.91;
    building.costUpgradeMultiplier = 1.05;
    building.CreateHTML();

    building = new Building(BuildingsNameEnum.CopperAssembler, BuildingsStringEnum.CopperAssembler, BuildingsEnum.CopperAssembler);
    building.baseIncreases[CurrencyEnum.CopperGear] = 1;
    building.baseDecreases[CurrencyEnum.CopperIngot] = 1;
    building.baseUpgradeCost[CurrencyEnum.CopperIngot] = 10;
    building.baseUpgradeCost[CurrencyEnum.CopperGear] = 20;
    building.buyCost[CurrencyEnum.CopperIngot] = 100;
    building.rowNumberId = 0;
    building.increaseMultiplier = 0.80;
    building.decreaseMultiplier = 0.81;
    building.costUpgradeMultiplier = 1.05;
    building.CreateHTML();
    //#endregion

    //#region Silver
    // building = new Building(BuildingsNameEnum.SilverMine, BuildingsStringEnum.SilverMine, BuildingsEnum.SilverMine);
    // building.baseIncreases[CurrencyEnum.Silver] = 0.1;
    // building.baseUpgradeCost[CurrencyEnum.Silver] = 2;
    // building.baseUpgradeCost[CurrencyEnum.CopperIngot] = 2;
    // building.buyCost[CurrencyEnum.CopperIngot] = 2;
    // building.rowNumberId = 1;
    // building.increaseMultiplier = 0.99;
    // building.decreaseMultiplier = 0.99;
    // building.costUpgradeMultiplier = 1.05;
    // building.CreateHTML();
    //#endregion
}
