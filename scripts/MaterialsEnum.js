/**
 * Список материалов
 * @type {{Silver: number, CopperIngot: number, Copper: number, SilverIngot: number}}
 */
export const CurrencyEnum = {
    Copper: 1,
    CopperIngot: 2,
    CopperGear: 3,
    Silver: 100,
    SilverIngot: 101
};

/**
 * Список построек
 * @type {{SilverMine: number, CopperSmelter: number, SilverSmelter: number, CopperMine: number}}
 */
export const BuildingsEnum = {
    CopperMine: 1,
    CopperSmelter: 2,
    CopperAssembler: 3,
    SilverMine: 100,
    SilverSmelter: 101
};

/**
 * Локализация построек
 */
export const BuildingsNameEnum = {
    CopperMine: "Медная шахта",
    CopperSmelter: "Медная плавильня",
    CopperAssembler: "Медный сборочный автомат",
    SilverMine: "Серебряная шахта",
    SilverSmelter: "Серебряная плавильня"
};

/**
 * Стринговые идентификаторы построек
 * @type {{SilverMine: string, CopperSmelter: string, SilverSmelter: string, CopperMine: string, CopperAssembler: string}}
 */
export const BuildingsStringEnum = {
    CopperMine: "CopperMine",
    CopperSmelter: "CopperSmelter",
    CopperAssembler: "CopperAssembler",
    SilverMine: "SilverMine",
    SilverSmelter: "SilverSmelter"
};
