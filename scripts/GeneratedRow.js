export function GeneratedRow() {
    const mainSheet = document.getElementById("main-sheet");
    const fragment = document.createDocumentFragment();

    for (let i = 0; i < 10; i++) {
        const divTag = document.createElement("div");
        divTag.className = "flex-container row";
        divTag.id = `rowProduceChain${i}`;

        fragment.appendChild(divTag);
    }
    mainSheet.appendChild(fragment);
}
