import {GeneratedRow} from "./GeneratedRow.js";
import {savebleGameData} from "./script.js";
import {CreateHtmlBuildings} from "./CreateHtmlBuildings.js";
import {BuildingsStringEnum, CurrencyEnum} from "./MaterialsEnum.js";

export function InitializeGame() {
    GeneratedRow();
    CreateHtmlBuildings();
    
    savebleGameData.Currencies[CurrencyEnum.Copper] = 10;
    document.getElementById("autoBuy" + BuildingsStringEnum.CopperMine).checked = true;
}
