import {savebleGameData, globalGameData} from "./script.js";

/**
 * Повышение уровня постройки
 * @param instanceID
 */
export function Upgrade(instanceID) {
    let buildingLvl = savebleGameData.BuildingsLevel;
    let buildingMaxLvl = savebleGameData.BuildingsMaxLevel;
    
    let building = GetBuildingById(instanceID);
    let isEnough = true;
    let buildingType = building.buildingType;
    
    if (buildingLvl[buildingType] !== buildingMaxLvl[buildingType]) {
        return;
    }
    
    for (let i = 0; i < savebleGameData.Currencies.length; i++) {
        if (building.upgradeCost[i] > savebleGameData.Currencies[i]) {
            isEnough = false;
        }
    }

    if (isEnough === true) {
        for (let i = 0; i < savebleGameData.Currencies.length; i++) {
            savebleGameData.Currencies[i] -= building.upgradeCost[i];
        }

        buildingMaxLvl[buildingType]++;
        buildingLvl[buildingType] = buildingMaxLvl[buildingType];
        
        building.UpdateLevelValues();
    }

    Calculate(building);
}

/**
 * 
 * @param building
 */
function Calculate(building) {
    CalculateProfit();
    building.UpdateUI();
}

/**
 * Вычислить профит прирост - расход
 */
function CalculateProfit() {
    let profit = globalGameData.currenciesProfit;
    profit.fill(0);
    for (let i = 0; i < globalGameData.buildingsInstances.length; i++) {
        for (let j = 0; j < profit.length; j++) {
            profit[j] += globalGameData.buildingsInstances[i].increases[j] - globalGameData.buildingsInstances[i].decreases[j];
        }
    }
}

/**
 * Получить постройку по идентификатору
 * @param id  number
 * @returns {building}
 */
function GetBuildingById(id) {
    return globalGameData.buildingsInstances[id];
}

/**
 * Даунгрейд постройки
 * @param instanceId
 */
export function SetInstanceCurrentMinusLvl(instanceId) {
    let building = GetBuildingById(instanceId);
    let isAvailable = CheckSetInstanceCurrentMinusLvl(building.buildingType);
    if (isAvailable) {
        savebleGameData.BuildingsLevel[building.buildingType]--;
        building.UpdateLevelValues();
        Calculate(building);
    }
}

/**
 * Повышение уровня постройки
 * @param instanceId
 */
export function SetInstanceCurrentPlusLvl(instanceId) {
    let building = GetBuildingById(instanceId);
    let isAvailable = CheckSetInstanceCurrentPlusLvl(building.buildingType);
    if (isAvailable) {
        savebleGameData.BuildingsLevel[building.buildingType]++;
        building.UpdateLevelValues();
        Calculate(building);
    }
}

/**
 * Проверка на установку текущего лвла
 * @param buildingType
 */
export function CheckSetInstanceCurrentMinusLvl(buildingType) {
    if (savebleGameData.BuildingsLevel[buildingType] > 0) {
        return true;
    }
}

/**
 * Проверка на установку текущего лвла
 * @param buildingType
 */
export function CheckSetInstanceCurrentPlusLvl(buildingType) {
    if (savebleGameData.BuildingsMaxLevel[buildingType] > savebleGameData.BuildingsLevel[buildingType]) {
        return true;
    }
}