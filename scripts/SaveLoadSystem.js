import {UpdateUI} from "./UpdateUI.js";
import {savebleGameData, globalGameData} from "./script.js";

let countdown = 30;
let showExport = 0;
let exportTimer = 0;

function saveGame() {
    localStorage.setItem('sqidle', JSON.stringify(savebleGameData));
}

function loadGame() {
    let gameTemp = JSON.parse(localStorage.getItem('sqidle'));
    for (let propertyName in gameTemp) {
        savebleGameData[propertyName] = gameTemp[propertyName];
    }
    for (let i = 0; i < globalGameData.buildingsInstances.length; i++) {
        globalGameData.buildingsInstances[i].UpdateLevelValues();
        globalGameData.buildingsInstances[i].UpdateUI();
    }
    UpdateUI();
}

function exportGame() {
    exportTimer = setInterval(exportCountdown, 1000);
    document.getElementById("divLblExport").innerHTML = btoa(JSON.stringify(game));
    showExport = 1;
    UpdateUI();
}

function exportCountdown() {
    if (countdown > 0) {
        countdown = countdown - 1;
    } else {
        clearTimeout(exportTimer);
        countdown = 30;
        showExport = 0;
        UpdateUI();
    }
}

function importGame() {
    let importString = prompt('Введите длинную строку экспорта'), gameTemp;
    gameTemp = JSON.parse(atob(importString));
    for (let propertyName in gameTemp) {
        savebleGameData[propertyName] = gameTemp[propertyName];
    }
    UpdateUI();
}
