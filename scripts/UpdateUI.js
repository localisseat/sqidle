import {globalGameData} from "./script.js";

export function UpdateUI() {
    for (let i = 0; i < globalGameData.buildingsInstances.length; i++) {
        globalGameData.buildingsInstances[i].Execute();
    }

    for (let i = 0; i < globalGameData.buildingsInstances.length; i++) {
        globalGameData.buildingsInstances[i].UpdateUI();
    }
}
